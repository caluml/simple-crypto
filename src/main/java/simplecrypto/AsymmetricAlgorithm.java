package simplecrypto;

public enum AsymmetricAlgorithm {

  RSA_2048_SHA256withRSA("RSA", 2048, "SHA256withRSA");

  private final String algorithm;
  private final int bits;
  private final String sigAlgo;

  AsymmetricAlgorithm(String algorithm, int bits, String sigAlgo) {
    this.algorithm = algorithm;
    this.bits = bits;
    this.sigAlgo = sigAlgo;
  }

  public String getAlgorithm() {
    return algorithm;
  }

  public int getBits() {
    return bits;
  }

  public String getSigAlgo() {
    return sigAlgo;
  }
}
