package simplecrypto;

import simplecrypto.symmetric.SymmetricKey;
import simplecrypto.symmetric.aes.AesKey;

import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class DhKeyExchanger {

  private KeyAgreement keyAgreement;

  public static DhKeyExchanger get() {
    return new DhKeyExchanger();
  }

  public byte[] init(int bits) {
    try {
      KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DH");
      keyPairGenerator.initialize(bits);
      KeyPair dhKeyPair = keyPairGenerator.generateKeyPair();

      keyAgreement = KeyAgreement.getInstance("DH");
      keyAgreement.init(dhKeyPair.getPrivate());

      return dhKeyPair.getPublic().getEncoded();
    } catch (NoSuchAlgorithmException | InvalidKeyException e) {
      throw new RuntimeException(e);
    }
  }

  public byte[] init(byte[] alicePubKeyEnc) {
    try {
      KeyFactory keyFactory = KeyFactory.getInstance("DH");
      X509EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(alicePubKeyEnc);
      PublicKey alicePubKey = keyFactory.generatePublic(encodedKeySpec);

      DHParameterSpec dhParamFromAlicePubKey = ((DHPublicKey) alicePubKey).getParams();
      KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DH");
      keyPairGenerator.initialize(dhParamFromAlicePubKey);
      KeyPair dhKeyPair = keyPairGenerator.generateKeyPair();

      keyAgreement = KeyAgreement.getInstance("DH");
      keyAgreement.init(dhKeyPair.getPrivate());

      return dhKeyPair.getPublic().getEncoded();
    } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidKeySpecException | InvalidAlgorithmParameterException e) {
      throw new RuntimeException(e);
    }
  }

  public SymmetricKey finalPhase(byte[] pubKeyEnc) {
    try {
      KeyFactory keyFactory = KeyFactory.getInstance("DH");
      PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(pubKeyEnc));
      keyAgreement.doPhase(publicKey, true);
      byte[] sharedSecret = keyAgreement.generateSecret();
      byte[] key = new byte[16];
      System.arraycopy(sharedSecret, 0, key, 0, 16);
      return new AesKey(key);
    } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidKeySpecException e) {
      throw new RuntimeException(e);
    }
  }
}
