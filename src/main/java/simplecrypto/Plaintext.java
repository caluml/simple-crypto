package simplecrypto;

import simplecrypto.symmetric.ByteWrapper;

public class Plaintext implements ByteWrapper {

  private final byte[] data;

  public Plaintext(byte[] data) {
    this.data = data;
  }

  @Override
  public byte[] getBytes() {
    return data;
  }
}
