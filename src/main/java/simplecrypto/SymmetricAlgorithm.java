package simplecrypto;

public enum SymmetricAlgorithm {

  AES_128_GCM_NOPADDING("AES", 128, "AES/GCM/NoPadding");

  private final String algorithm;
  private final int bits;
  private final String transformation;

  SymmetricAlgorithm(String algorithm, int bits, String transformation) {
    this.algorithm = algorithm;
    this.bits = bits;
    this.transformation = transformation;
  }

  public String getAlgorithm() {
    return algorithm;
  }

  public int getBits() {
    return bits;
  }

  public String getTransformation() {
    return transformation;
  }
}
