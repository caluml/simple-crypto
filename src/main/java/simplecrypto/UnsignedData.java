package simplecrypto;

import simplecrypto.symmetric.ByteWrapper;

public class UnsignedData implements ByteWrapper {

  private final byte[] data;

  public UnsignedData(byte[] data) {
    this.data = data;
  }

  @Override
  public byte[] getBytes() {
    return data;
  }
}
