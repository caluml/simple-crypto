package simplecrypto.asymmetric;

import simplecrypto.AsymmetricAlgorithm;
import simplecrypto.UnsignedData;
import simplecrypto.asymmetric.rsa.RsaKeyPair;
import simplecrypto.asymmetric.rsa.RsaSignature;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class AsymmetricCrypto {

  private final String algorithm;
  private final int bits;
  private final String sigAlgo;

  public static AsymmetricCrypto get(AsymmetricAlgorithm asymmetricAlgorithm) {
    return new AsymmetricCrypto(asymmetricAlgorithm.getAlgorithm(), asymmetricAlgorithm.getBits(), asymmetricAlgorithm.getSigAlgo());
  }

  public AsymmetricCrypto(String algorithm, int bits, String sigAlgo) {
    this.algorithm = algorithm;
    this.bits = bits;
    this.sigAlgo = sigAlgo;
  }

  public AsymmetricKeyPair generateKeyPair() {
    try {
      KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(algorithm);
      keyPairGenerator.initialize(bits, SecureRandom.getInstanceStrong());
      KeyPair keyPair = keyPairGenerator.generateKeyPair();
      return new RsaKeyPair(keyPair.getPrivate().getEncoded(), keyPair.getPublic().getEncoded());
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  public Signature sign(AsymmetricPrivateKey asymmetricPrivateKey, UnsignedData unsignedData) {
    try {
      java.security.Signature signature = java.security.Signature.getInstance(sigAlgo);
      PrivateKey privateKey = getPrivateKey(asymmetricPrivateKey.getBytes());
      signature.initSign(privateKey);

      signature.update(unsignedData.getBytes());
      return new RsaSignature(signature.sign());
    } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidKeySpecException | SignatureException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Throws {@link InvalidAsymmetricSignatureException} if not a valid signature
   */
  public void verify(AsymmetricPublicKey asymmetricPublicKey, UnsignedData unsignedData, Signature signature) {
    try {
      java.security.Signature ver = java.security.Signature.getInstance(sigAlgo);
      PublicKey publicKey = getPublicKey(asymmetricPublicKey.getBytes());
      ver.initVerify(publicKey);

      ver.update(unsignedData.getBytes());
      if (!ver.verify(signature.getSignature())) {
        throw new InvalidAsymmetricSignatureException();
      }
    } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
      throw new RuntimeException(e);
    }
  }

  private static PrivateKey getPrivateKey(byte[] bytes) throws NoSuchAlgorithmException, InvalidKeySpecException {
    PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(bytes);
    KeyFactory kf = KeyFactory.getInstance("RSA");
    return kf.generatePrivate(pkcs8EncodedKeySpec);
  }

  private static PublicKey getPublicKey(byte[] bytes) {
    try {
      X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(bytes);
      KeyFactory keyFactory = KeyFactory.getInstance("RSA");
      return keyFactory.generatePublic(x509EncodedKeySpec);
    } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
      throw new RuntimeException(e);
    }
  }

  private static byte[] getRandomBytes(int numBytes) {
    try {
      byte[] bytes = new byte[numBytes];
      SecureRandom.getInstanceStrong().nextBytes(bytes);
      return bytes;
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }
}
