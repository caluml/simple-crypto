package simplecrypto.asymmetric;

public interface AsymmetricKeyPair {

  AsymmetricPrivateKey getPrivate();

  AsymmetricPublicKey getPublic();

}
