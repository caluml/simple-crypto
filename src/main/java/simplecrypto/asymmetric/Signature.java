package simplecrypto.asymmetric;

public interface Signature {

  byte[] getSignature();
}
