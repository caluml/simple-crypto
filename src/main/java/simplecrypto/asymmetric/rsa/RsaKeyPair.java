package simplecrypto.asymmetric.rsa;

import simplecrypto.asymmetric.AsymmetricKeyPair;
import simplecrypto.asymmetric.AsymmetricPrivateKey;
import simplecrypto.asymmetric.AsymmetricPublicKey;

public class RsaKeyPair implements AsymmetricKeyPair {

  private final byte[] priv;
  private final byte[] pub;

  public RsaKeyPair(byte[] priv, byte[] pub) {
    this.priv = priv;
    this.pub = pub;
  }

  @Override
  public AsymmetricPrivateKey getPrivate() {
    return new RsaPrivateKey(priv);
  }

  @Override
  public AsymmetricPublicKey getPublic() {
    return new RsaPublicKey(pub);
  }
}
