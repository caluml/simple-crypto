package simplecrypto.asymmetric.rsa;

import simplecrypto.asymmetric.AsymmetricPrivateKey;

public class RsaPrivateKey implements AsymmetricPrivateKey {

  private final byte[] bytes;

  public RsaPrivateKey(byte[] bytes) {
    this.bytes = bytes;
  }

  @Override
  public byte[] getBytes() {
    return bytes;
  }
}
