package simplecrypto.asymmetric.rsa;

import simplecrypto.asymmetric.AsymmetricPublicKey;

public class RsaPublicKey implements AsymmetricPublicKey {

  private final byte[] bytes;

  public RsaPublicKey(byte[] bytes) {
    this.bytes = bytes;
  }

  @Override
  public byte[] getBytes() {
    return bytes;
  }
}
