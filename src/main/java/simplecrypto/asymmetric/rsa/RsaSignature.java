package simplecrypto.asymmetric.rsa;

import simplecrypto.asymmetric.Signature;

public class RsaSignature implements Signature {

  private final byte[] signature;

  public RsaSignature(byte[] signature) {
    this.signature = signature;
  }

  @Override
  public byte[] getSignature() {
    return signature;
  }
}
