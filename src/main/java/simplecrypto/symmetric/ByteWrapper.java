package simplecrypto.symmetric;

public interface ByteWrapper {

  byte[] getBytes();
}
