package simplecrypto.symmetric;

public class Decrypted implements ByteWrapper {

  private final byte[] bytes;

  public Decrypted(byte[] bytes) {
    this.bytes = bytes;
  }

  @Override
  public byte[] getBytes() {
    return bytes;
  }
}
