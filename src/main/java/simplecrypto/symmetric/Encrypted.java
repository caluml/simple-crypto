package simplecrypto.symmetric;

public class Encrypted {

  private final byte[] data;
  private final byte[] iv;

  public Encrypted(byte[] data, byte[] iv) {
    this.data = data;
    this.iv = iv;
  }

  public byte[] getData() {
    return data;
  }

  public byte[] getIv() {
    return iv;
  }
}
