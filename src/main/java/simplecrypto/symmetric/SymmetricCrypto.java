package simplecrypto.symmetric;

import simplecrypto.Plaintext;
import simplecrypto.SymmetricAlgorithm;
import simplecrypto.symmetric.aes.AesKey;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class SymmetricCrypto {

  private final String algorithm;
  private final int bits;
  private final String transformation;

  public static SymmetricCrypto get(SymmetricAlgorithm symmetricAlgorithm) {
    return new SymmetricCrypto(symmetricAlgorithm.getAlgorithm(), symmetricAlgorithm.getBits(), symmetricAlgorithm.getTransformation());
  }

  public SymmetricCrypto(String algorithm, int bits, String transformation) {
    this.algorithm = algorithm;
    this.bits = bits;
    this.transformation = transformation;
  }

  public SymmetricKey generateKey() {
    try {
      KeyGenerator keyGen = KeyGenerator.getInstance(algorithm);
      keyGen.init(bits, SecureRandom.getInstanceStrong());
      SecretKey secretKey = keyGen.generateKey();
      return new AesKey(secretKey.getEncoded());
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  public Encrypted encrypt(SymmetricKey symmetricKey, Plaintext plaintext) {
    try {
      Key secretKey = toSecretKey(symmetricKey.getBytes());
      Cipher cipher = Cipher.getInstance(transformation);
      byte[] iv = getRandomBytes(16);
      cipher.init(Cipher.ENCRYPT_MODE, secretKey, new GCMParameterSpec(bits, iv));
      byte[] encrypted = cipher.doFinal(plaintext.getBytes());
      return new Encrypted(encrypted, iv);
    } catch (GeneralSecurityException e) {
      throw new RuntimeException(e);
    }
  }

  public Decrypted decrypt(SymmetricKey symmetricKey, Encrypted encrypted) {
    try {
      Key secretKey = toSecretKey(symmetricKey.getBytes());
      Cipher cipher = Cipher.getInstance(transformation);
      cipher.init(Cipher.DECRYPT_MODE, secretKey, new GCMParameterSpec(bits, encrypted.getIv()));
      return new Decrypted(cipher.doFinal(encrypted.getData()));
    } catch (GeneralSecurityException e) {
      throw new RuntimeException(e);
    }
  }

  private static byte[] getRandomBytes(int numBytes) {
    try {
      byte[] bytes = new byte[numBytes];
      SecureRandom.getInstanceStrong().nextBytes(bytes);
      return bytes;
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  private SecretKey toSecretKey(byte[] bytes) {
    return new SecretKeySpec(bytes, algorithm);
  }

}
