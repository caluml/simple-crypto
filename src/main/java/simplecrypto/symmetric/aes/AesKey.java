package simplecrypto.symmetric.aes;

import simplecrypto.symmetric.SymmetricKey;

import java.util.Arrays;

public class AesKey implements SymmetricKey {

  private final byte[] bytes;

  public AesKey(byte[] bytes) {
    this.bytes = bytes;
  }

  @Override
  public byte[] getBytes() {
    return bytes;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AesKey aesKey = (AesKey) o;
    return Arrays.equals(bytes, aesKey.bytes);
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(bytes);
  }
}
