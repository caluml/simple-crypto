package simplecrypto;

import org.junit.Test;
import simplecrypto.asymmetric.AsymmetricCrypto;
import simplecrypto.asymmetric.AsymmetricKeyPair;
import simplecrypto.asymmetric.Signature;
import simplecrypto.symmetric.*;

import static org.assertj.core.api.Assertions.assertThat;

public class FullTest {

  @Test
  public void End_to_end() {
    Plaintext plaintext = new Plaintext("SOME TEXT".getBytes());
    SymmetricCrypto symmetricCrypto = SymmetricCrypto.get(SymmetricAlgorithm.AES_128_GCM_NOPADDING);
    AsymmetricCrypto asymmetricCrypto = AsymmetricCrypto.get(AsymmetricAlgorithm.RSA_2048_SHA256withRSA);

    SymmetricKey symmetricKey = symmetricCrypto.generateKey();
    AsymmetricKeyPair asymmetricKeyPair = asymmetricCrypto.generateKeyPair();

    Encrypted encrypted = symmetricCrypto.encrypt(symmetricKey, plaintext);
    UnsignedData unsignedData = new UnsignedData(encrypted.getData());
    Signature signature = asymmetricCrypto.sign(asymmetricKeyPair.getPrivate(), unsignedData);

    asymmetricCrypto.verify(asymmetricKeyPair.getPublic(), unsignedData, signature);

    Decrypted decrypted = symmetricCrypto.decrypt(symmetricKey, encrypted);

    assertThat(decrypted.getBytes()).containsExactly(plaintext.getBytes());
  }

  @Test
  public void Can_negotiate_keys() {
    DhKeyExchanger alice = DhKeyExchanger.get();
    DhKeyExchanger bob = DhKeyExchanger.get();

    byte[] alicePubKey = alice.init(2048);
    // Transfer Alice's public key to Bob

    byte[] bobPubKey = bob.init(alicePubKey);
    SymmetricKey bobSecretKey = bob.finalPhase(alicePubKey);
    // Transfer Bob's public key to Alice

    SymmetricKey aliceSecretKey = alice.finalPhase(bobPubKey);

    assertThat(aliceSecretKey.getBytes()).containsExactly(bobSecretKey.getBytes());


    Plaintext plaintext = new Plaintext("Drink your Ovaltine".getBytes());
    SymmetricCrypto symmetricCrypto = SymmetricCrypto.get(SymmetricAlgorithm.AES_128_GCM_NOPADDING);

    Encrypted encrypted = symmetricCrypto.encrypt(aliceSecretKey, plaintext);

    Decrypted decrypted = symmetricCrypto.decrypt(bobSecretKey, encrypted);

    assertThat(decrypted.getBytes()).containsExactly(plaintext.getBytes());
  }
}
