package simplecrypto.asymmetric;

import org.junit.Test;
import simplecrypto.AsymmetricAlgorithm;
import simplecrypto.UnsignedData;

public class AsymmetricCryptoTest {

  @Test
  public void Valid_signature() {
    AsymmetricCrypto asymmetricCrypto = AsymmetricCrypto.get(AsymmetricAlgorithm.RSA_2048_SHA256withRSA);
    AsymmetricKeyPair asymmetricKeyPair = asymmetricCrypto.generateKeyPair();
    UnsignedData unsignedData = new UnsignedData("some data".getBytes());

    Signature signature = asymmetricCrypto.sign(asymmetricKeyPair.getPrivate(), unsignedData);

    asymmetricCrypto.verify(asymmetricKeyPair.getPublic(), unsignedData, signature);

    // No exception thrown
  }

  @Test(expected = InvalidAsymmetricSignatureException.class)
  public void Valid_signature_data_changed() {
    AsymmetricCrypto asymmetricCrypto = AsymmetricCrypto.get(AsymmetricAlgorithm.RSA_2048_SHA256withRSA);
    AsymmetricKeyPair asymmetricKeyPair = asymmetricCrypto.generateKeyPair();
    UnsignedData unsignedData = new UnsignedData("some data".getBytes());

    Signature signature = asymmetricCrypto.sign(asymmetricKeyPair.getPrivate(), unsignedData);

    asymmetricCrypto.verify(asymmetricKeyPair.getPublic(), new UnsignedData("some other data".getBytes()), signature);
  }

  @Test(expected = InvalidAsymmetricSignatureException.class)
  public void Invalid_signature() {
    AsymmetricCrypto asymmetricCrypto = AsymmetricCrypto.get(AsymmetricAlgorithm.RSA_2048_SHA256withRSA);
    UnsignedData unsignedData = new UnsignedData("some data".getBytes());

    AsymmetricKeyPair asymmetricKeyPair1 = asymmetricCrypto.generateKeyPair();
    // Make signature with keypair 1
    Signature signature = asymmetricCrypto.sign(asymmetricKeyPair1.getPrivate(), unsignedData);

    AsymmetricKeyPair asymmetricKeyPair2 = asymmetricCrypto.generateKeyPair();
    // Verify with keypair 2
    asymmetricCrypto.verify(asymmetricKeyPair2.getPublic(), unsignedData, signature);
  }
}
