package simplecrypto.keyexchange;

import org.junit.Test;
import simplecrypto.DhKeyExchanger;
import simplecrypto.symmetric.SymmetricKey;
import simplecrypto.symmetric.aes.AesKey;

import javax.crypto.SecretKey;

import static org.assertj.core.api.Assertions.assertThat;

public class DhKeyExchangerTest {

  @Test
  public void Can_negotiate_keys() {
    DhKeyExchanger alice = DhKeyExchanger.get();
    DhKeyExchanger bob = DhKeyExchanger.get();

    byte[] alicePubKey = alice.init(2048);
    // Transfer Alice's public key to Bob

    byte[] bobPubKey = bob.init(alicePubKey);
    SymmetricKey bobSecretKey = bob.finalPhase(alicePubKey);
    // Transfer Bob's public key to Alice

    SymmetricKey aliceSecretKey = alice.finalPhase(bobPubKey);

    assertThat(aliceSecretKey.getBytes()).containsExactly(bobSecretKey.getBytes());
  }
}
