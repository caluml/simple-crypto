package simplecrypto.symmetric;

import org.junit.Test;
import simplecrypto.Plaintext;
import simplecrypto.SymmetricAlgorithm;

import static org.assertj.core.api.Assertions.assertThat;

public class SymmetricCryptoTest {

  @Test
  public void End_to_end() {
    SymmetricCrypto symmetricCrypto = SymmetricCrypto.get(SymmetricAlgorithm.AES_128_GCM_NOPADDING);
    SymmetricKey symmetricKey = symmetricCrypto.generateKey();
    Plaintext plaintext = new Plaintext("SOME TEXT".getBytes());

    Encrypted encrypted = symmetricCrypto.encrypt(symmetricKey, plaintext);

    Decrypted decrypted = symmetricCrypto.decrypt(symmetricKey, encrypted);

    assertThat(decrypted.getBytes()).containsExactly(plaintext.getBytes());
  }
}
